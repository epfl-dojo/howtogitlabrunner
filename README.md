# HowToGitLabRunner

The goal of this project is to demonstrate how to use
[GitLab-Runner](https://docs.gitlab.com/runner) with a very simple use case:
transform this README.md into README.pdf with [pandoc](https://pandoc.org/).

## Setup the runner

First of all, you need a runner on a machine. The installation process is
described here: https://docs.gitlab.com/runner/install/

It create the gitlab-runner user on your system. On some Linux, you may want to
remove the `~/.bash_logout` file to avoid the issue
[4092](https://gitlab.com/gitlab-org/gitlab-runner/issues/4092) — thanks me
later.

The last step explain how to register the runner with `sudo gitlab-runner
register`. Answer the questions (the runner key is found on your project >
settings > CI/CD > runner). You may want to check the runner options (for
example "Run untagged jobs"). It will create the
`/etc/gitlab-runner/config.toml` file which looks like that:

```
concurrent = 1
check_interval = 0

[session_server]
  session_timeout = 1800

[[runners]]
  name = "X1"
  url = "https://gitlab.epfl.ch"
  token = "tQqmy2TFSZoJQfm-83_W"
  executor = "shell"
  [runners.custom_build_dir]
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
```

## next 2
